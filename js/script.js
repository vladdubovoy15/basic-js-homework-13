"use strict";

const btn = document.querySelector("#button");
const theme = document.querySelector(".wrapper");

if(localStorage.getItem("btn") === "change"){
    theme.classList.add("change"); 
}

function changeTheme(){
    if(document.querySelector(".change")){
        theme.classList.remove("change");
        localStorage.removeItem("btn");
    } else {
        theme.classList.add("change"); 
        localStorage.setItem("btn", "change");
    }
}

btn.onclick = changeTheme;

